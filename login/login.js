/**
 * Created by Xurxo on 03/02/2015.
 */
'use strict';

angular.module('myApp.login', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login/login.html',
            controller: 'login_control'
        });
    }])
    .controller('login_control', ['$scope', '$http', function ($scope, $http) {
        $scope.login = function () {
            //alert("ASD");

            //$http.get('/api/auth').
            //    success(function(data, status, headers, config) {
            //        alert("HEEEY");
            //        // this callback will be called asynchronously
            //        // when the response is available
            //    }).
            //    error(function(data, status, headers, config) {
            //        alert("HEEEY");
            //        // called asynchronously if an error occurs
            //        // or server returns response with an error status.
            //    });

            $http.post('/api/auth', {
                user: $scope.user.name,
                pass: $scope.user.password}
            ).
                success(function(data, status, headers, config) {
                    alert(data);
                    // this callback will be called asynchronously
                    // when the response is available
                }).
                error(function(data, status, headers, config) {
                    alert(data);
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });


            //$http({
            //    method: 'POST',
            //    url: '/api/auth',
            //    data: $scope.user.name
            //})
            //    .success(function (response) {
            //        alert("OK");
            //    })
        };
    }])
    .service();