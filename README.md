Step by step:

    1. Run MongoDB:
        "C:\Program Files\MongoDB 2.6 Standard\bin\mongod.exe" --dbpath D:\Mongo\data

    2. Run the NodeJS Server:
        node server_node.js

    3. The Web Application is listening at localhost:8000