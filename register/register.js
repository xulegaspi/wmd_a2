/**
 * Created by Xurxo on 04/02/2015.
 */
'use strict';

angular.module('myApp.register', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/register', {
            templateUrl: 'register/register.html',
            controller: 'register_control'
        });
    }])
    .controller('register_control', ['$scope', '$http', function ($scope, $http) {
        $scope.register = function () {

            $http.post('/api/register', {
                    user: $scope.user.name,
                    email: $scope.user.email,
                    pass: $scope.user.password}
            ).
                success(function(data, status, headers, config) {
                    alert(data);
                    // this callback will be called asynchronously
                    // when the response is available
                }).
                error(function(data, status, headers, config) {
                    alert(data);
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });

        };
    }])
    .service();