/**
 * Created by Xurxo on 04/02/2015.
 */
var sys = require('sys');
var http = require('http');
var path = require('path');
var url = require('url');
var fs = require('fs');

var mongoose = require('mongoose');

//var MongoClient = require('mongodb').MongoClient
//    , format = require('util').format;
//
//MongoClient.connect('mongodb://127.0.0.1:27017/test', function (err, db) {
//    if (err) {
//        throw err;
//    } else {
//        console.log("successfully connected to the database");
//    }
//    db.close();
//});

var User = mongoose.model('User', {
    name: String,
    email: String,
    pass: String
});

mongoose.connect('mongodb://localhost/test');

http.createServer(function (req, res) {
    // set up some routes
    var my_path = url.parse(req.url).pathname;
    var full_path = path.join(process.cwd(),my_path);

    switch(req.url) {

        case '/':
            fs.readFile('./index.html', function(err, file) {
                if(err) {
                    console.log("ERROR - " + req.url);
                } else {
                    console.log("[200] " + req.method + " to " + req.url);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write(file);
                    res.end();
                }
            });
            break;

        case '/index.html':
            console.log("[501] " + req.method + " to " + req.url);
            res.writeHead(501, "Not implemented", {'Content-Type': 'application/javascript'});
            res.end('<html><head><title>501 - Not implemented</title></head><body><h1>Not implemented!</h1></body></html>');
            break;
        case '/styles.css':
            fs.readFile(full_path, function(err, file) {
                if(err) {
                    console.log("ERROR");
                } else {
                    console.log("[200] " + req.method + " to " + req.url);
                    res.writeHead(200, {'Content-Type': 'text/css'});
                    res.write(file);
                    res.end();
                }
            });
            break;

        // Where authentication takes place
        case '/api/auth':
            req.on('data', function(chunk) {
                var data = chunk.toString();
                console.log("Received body data:");
                console.log(data);
                var data2 = JSON.parse(data);
                var user = data2['user'];
                var pass = data2['pass'];
                User.findOne({ name: user }, function (err, user) {
                    if (err) {
                        return callback(err, new me.ApiResponse({ success: false, extras: { msg: me.ApiMessages.DB_ERROR } }));
                    }
                    if (user) {




                        if() {
                    console.log("[200] " + req.method + " to " + req.url);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write("Logged in");
                    res.end();
                } else {
                    //mongoose.connect('mongodb://localhost/test');
                    User.find(function (err, users) {
                        if (err) return console.error(err);
                        console.log(users)
                    });
                    //mongoose.disconnect();
                    console.log("[200] " + req.method + " to " + req.url);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write("Incorrect");
                    res.end();
                }
                console.log("User: " + user + " ,Pass: " + pass);
            });
            //console.log("[200] " + req.method + " to " + req.url);
            //res.writeHead(200, {'Content-Type': 'text/html'});
            //res.write("{'data':'hola'}");
            //res.end();
            break;

        case '/api/register':
            req.on('data', function(chunk) {
                var data = chunk.toString();
                console.log("Received body data:");
                console.log(data);
                var data2 = JSON.parse(data);
                var user = data2['user'];
                var email = data2['email'];
                var pass = data2['pass'];
                //if(user == 'test' && pass == 'test') {

                var new_user = new User({
                    name: user,
                    email: email,
                    pass: pass
                });
                new_user.save(function (err) {
                    if (err) // ...
                        console.log('meow');
                });
                //mongoose.disconnect();

                    console.log("[200] " + req.method + " to " + req.url);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write("Registered: User " + user + ", with email: " + email);
                    res.end();
                //} else {
                //    console.log("[200] " + req.method + " to " + req.url);
                //    res.writeHead(200, {'Content-Type': 'text/html'});
                //    res.write("Incorrect");
                //    res.end();
                //}
                console.log("User: " + user + " ,Pass: " + pass);
            });
            break;

        default:
            fs.readFile(full_path, function(err, file) {
                if(err) {
                    console.log("ERROR - " + req.url);
                } else {
                    console.log("[200] " + req.method + " to " + req.url);
                    res.writeHead(200, {'Content-Type': 'application/javascript'});
                    res.write(file);
                    res.end();
                }
            });
    };
}).listen(8000); // listen on tcp port 8080 (all interfaces)