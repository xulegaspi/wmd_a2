/**
 * Created by Xurxo on 03/02/2015.
 */

var sys = require('sys');
var http = require('http');
var path = require('path');
var url = require('url');
var fs = require('fs');
//var connect = require('connect');
//var serveStatic = require('serve-static');
//
//connect().use(serveStatic(__dirname)).listen(8000);

//var html = fs.readFile('./index.html', function (err, data) {
//    if (err) {
//        console.log("ERROR");
//        throw err;
//    }
//    console.log(data);
//});

http.createServer(function (request, response) {

    var my_path = url.parse(request.url).pathname;
    var full_path = path.join(process.cwd(),my_path);

    fs.readFile(full_path, "binary", function(err, file) {
        if(err) {
            response.writeHeader(500, {"Content-Type": "text/plain"});
            response.write(err + ' ' + my_path + "\n");
            response.end();

        }
        else{
            response.writeHeader(200, {"Content-Type": "text/html"});
            response.write(file);
            response.write(request.data());
            response.end();
        }
    });

    //response.writeHead(200, {"Content-Type": "text/html"});
    //response.write(my_path);
    //response.end();
}).listen(8000);

